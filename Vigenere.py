#!/usr/bin/python3

import os
import argparse


class Vigenere:



	def __init__(self):
		pass



	@classmethod
	def Vigenere(cls, message, key, mode):
		
		lock = 0
		

		if isinstance(message, str):
			lock += 1


		if isinstance(key, str):
			lock += 1


		if isinstance(mode, bool):
			lock += 1


		if lock == 3:

			alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			answer = ""

			key = key.replace(" ", "")
			keyLen = len(key)
			keyI = 0

			for m in message:

				if m in (" ", "'", "!", "?"):
					answer += m

				if m.upper() in alphabet:

					symbol = [
						alphabet[(alphabet.find(m.upper()) - alphabet.find(key[keyI%keyLen].upper())) % 26],
						alphabet[(alphabet.find(m.upper()) + alphabet.find(key[keyI%keyLen].upper())) % 26]
					]

					answer += symbol[int(mode)]
					keyI += 1

			return answer


try:
	parser = argparse.ArgumentParser()

	parser.add_argument("-f", "--file", type=str)
	parser.add_argument("-s", "--string", type=str)
	parser.add_argument("-k", "--key", type=str)
	parser.add_argument("-e", "--encrypt", action="store_false")
	parser.add_argument("-d", "--decrypt", action="store_false")

	args = parser.parse_args()

	_file = args.file
	_string = args.string 
	_key = args.key
	_encrypt = args.encrypt
	_decrypt = args.decrypt


	if _string and _key and not _encrypt and _decrypt:
		print(Vigenere.Vigenere(_string, _key, True))


	if _string and _key and not _decrypt and _encrypt:
		print(Vigenere.Vigenere(_string, _key, False))


	if _file and _key and not _encrypt and _decrypt:

		if os.path.isfile(_file):
			if _file.lower().endswith((".txt")):

				filename = _file.split(".")[0] + "_encrypt.txt"

				with open(filename, "w") as erase:
					pass
				
				with open(_file, "r") as read:
					for line in read:
						with open(filename, "a+") as save:

							if line != "\n":
								save.write(Vigenere.Vigenere(line, _key, True) + "\n")
							else:
								save.write("\n")
			else:
				print("Please put the content in a .txt file")
		else:
			print("File not found or the path to the file not found.\n")
			print("Check if you have the reading rights.\n")


	if _file and _key and not _decrypt and _encrypt:

		if os.path.isfile(_file):
			if _file.lower().endswith((".txt")):

				filename = _file.split(".")[0] + "_decrypt.txt"

				with open(filename, "w") as erase:
					pass
				
				with open(_file, "r") as read:
					for line in read:
						with open(filename, "a+") as save:

							if line != "\n":
								save.write(Vigenere.Vigenere(line, _key, False) + "\n")
							else:
								save.write("\n")
			else:
				print("Please put the content in a .txt file")
		else:
			print("File not found or the path to the file not found.\n")
			print("Check if you have the reading rights.\n")
except:
	pass