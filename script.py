#!/usr/bin/python3

import sys
import os
from Vigenere import Vigenere

print("Encrypted :", Vigenere.Vigenere("Vigenere", "Cipher", True))
print("Decrypted :", Vigenere.Vigenere("XQVLRVTM", "Cipher", False))

if sys.platform.startswith('win32'):
	os.system("pause")