# Vigenere 
https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher

## Dependencies
* Python 3 (https://www.python.org/downloads/)

## Terminal 

### **Windows**
<pre>
    # Encrypts a message, a sentence
    Vigenere.py -s "Vigenere" -k "Cipher" -e
    
    # Deciphers a message, a sentence
    Vigenere.py -s "XQVLRVTM" -k "Cipher" -d
    
    # Encrypts the content of a file 
    Vigenere.py -f myfile -k "Cipher" -e
    
    # Decrypts the content of a file
    Vigenere.py -f myfile_encrypt.txt -k "Cipher" -d
</pre>

### **Linux**
same command as Windows except that the line as by "./Vigenere.py" without quotation marks follows parameters you want to put

### **Mac**
same command as Windows except that the line as by "python3 Vigenere.py" without quotation marks follows parameters you want to put

## Script 

Vigenere.Vigenere(Message, Key, Mode)

* Message (str) : The encryption or decryption message.
* Key (str) : The key for decrypting or encrypting the message.
* Mode (bool) : The mode allows either to encrypt or decrypt.
    * True | 1 : Encrypt
    * False | 0 : Decrypt

```python
    from Vigenere import Vigenere
    
    # Encrypt
    print(Vigenere.Vigenere("Vigenere", "Cipher", True))
    
    # Decrypt 
    print(Vigenere.Vigenere("XQVLRVTM", "Cipher", False))
```

### **Windows**
<pre>
    script.py or double click on the file.
</pre>

### **Linux**
```python
    #!/usr/bin/python3
    
    from Vigenere import Vigenere
```

<pre>
    chmod +x script.py
    ./script.py
</pre>

### **Mac**
<pre>
    python3 script.py
</pre>

## Contact me

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Mail :** mx.moreau1@gmail.com
* **Twitter :** mxmmoreau (https://twitter.com/mxmmoreau)